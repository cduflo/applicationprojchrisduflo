﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class CarFactory
    {
        public CarFactory() { }


        //Define Vehicle selection
        public static List<string> Vehicles = new List<string>() { "Sedan", "RaceCar", "SpaceShip" };


        public Car chooseCar(int xPos, int yPos, City city, Passenger passenger, string car)
        {
            switch (car)
            {
                case "sedan":
                    return new Sedan(xPos, yPos, city, null);
                case "racecar":
                    return new RaceCar(xPos, yPos, city, null);
                case "spaceship":
                    return new SpaceShip(xPos, yPos, city, null);
                default:
                    return new Sedan(xPos, yPos, city, null);
            }
        }
    }
}
