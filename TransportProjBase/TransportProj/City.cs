﻿
namespace TransportProj
{
    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
        }

        public Car AddCarToCity(int xPos, int yPos, string choice)
        {
            CarFactory factory = new CarFactory();
            Car car = factory.chooseCar(xPos, yPos, this, null, choice);

            return car;
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }


        public Building AddBuildingToCity(int XPos, int YPos)
        {
            Building building = new Building(XPos, YPos, this);

            return building;
        }

    }
}
