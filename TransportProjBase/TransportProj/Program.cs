﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;
            int BuildingsNumber = 3;
            string choice;
            List<Building> Buildings = new List<Building>();

            //Ask user for vehicle selection, and validate selection.
            while (true)
            {
                Console.WriteLine("Please select your vehicle:");

                foreach (var x in CarFactory.Vehicles)
                {
                    Console.WriteLine(x);
                }

                choice = Console.ReadLine().ToLower();

                if (CarFactory.Vehicles.Any(s => s.Equals(choice, StringComparison.OrdinalIgnoreCase)))
                {
                    break;
                }
                else
                {
                    Console.WriteLine("Sorry, I didn't catch that. Please type your selection again.");
                }
            }


            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), choice);
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
            for (var i = 0; i < BuildingsNumber; i++)
            {
                int buildingXPos = rand.Next(CityLength - 1);
                int buildingYPos = rand.Next(CityWidth - 1);

                //ensures Buildings aren't created on top of Passenger or Vehicle
                if (!(buildingXPos == car.XPos && buildingYPos == car.YPos || buildingXPos == passenger.StartingXPos && buildingYPos == passenger.StartingYPos || buildingXPos == passenger.DestinationXPos && buildingYPos == passenger.DestinationYPos))
                {
                    Buildings.Add(MyCity.AddBuildingToCity(buildingXPos, buildingYPos));
                }
            }

            //Used to validate testing
            Console.WriteLine("Car: " + car.XPos + "," + car.YPos);
            Console.WriteLine("Passenger Start: " + passenger.StartingXPos + "," + passenger.StartingYPos);
            Console.WriteLine("Passenger Destination: " + passenger.DestinationXPos + "," + passenger.DestinationYPos);
            CityPosition.Draw(CityLength, CityWidth, car, passenger, Buildings);

            while (!passenger.IsAtDestination())
            {
                Tick(car, passenger, Buildings, CityLength, CityWidth);

            }

            passenger.GetOutOfCar();

            //Keep console open for validation
            Console.ReadLine();

            return;
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger, List<Building> buildings, int cityLength, int cityWidth)
        {
            if (car.XPos == passenger.StartingXPos && car.YPos == passenger.StartingYPos && passenger.Car == null)
            {
                passenger.GetInCar(car);
            }
            else if (passenger.Car == null)
            {
                Car.Move(car, buildings, passenger.StartingXPos, passenger.StartingYPos, cityLength, cityWidth, passenger);
            }
            else
            {
                Car.Move(car, buildings, passenger.DestinationXPos, passenger.DestinationYPos, cityLength, cityWidth, passenger);
            }

        }

    }
}
