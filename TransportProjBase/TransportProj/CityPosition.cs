﻿
using System;
using System.Collections.Generic;

namespace TransportProj
{
    public class CityPosition
    {
        public Car Car { get; set; }
        public Passenger Passenger { get; set; }


       public static void Draw(int cityX, int cityY, Car car, Passenger passenger, List<Building> buildings)
        {
            char[,] render = new char[cityX, cityY];

            //Create Map
            for (var x = 0; x < cityX; x++)
                for (var y = 0; y < cityY; y++)
                    render[x, y] = '.';

            //Add Passenger to Map
            if (passenger.Car == null)
                render[passenger.StartingXPos, passenger.StartingYPos] = 'P';

            //Add Vehicle to Map
            render[car.XPos, car.YPos] = 'V';

            //Add Buildings to Map
            for (var i = 0; i <buildings.Count; i++)
            {
                render[buildings[i].XPos, buildings[i].YPos] = 'B';
            }

            //Draw map on console
            for (var y = cityY - 1; y >= 0; y--)
            {
                for (var x = 0; x < cityX; x++)
                {
                    Console.Write(render[x, y]);
                }
                Console.WriteLine();
            }
        }
    }
}
