﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {

        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override int Speed
        {
            get
            {
                return 1;
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
