﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class Building
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public City City { get; private set; }

        public Building(int xPos, int yPos, City city)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
        }
    }
}
