﻿using System;
using System.Collections.Generic;

namespace TransportProj
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }
        public abstract int Speed { get; }

        private static int stuckCount = 0;

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }


        public void MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos++;
            }
        }

        public void MoveDown()
        {
            if (YPos > 0)
            {
                YPos--;
            }
        }

        public void MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos++;
            }
        }

        public void MoveLeft()
        {
            if (XPos > 0)
            {
                XPos--;
            }
        }

        public static void Move(Car car, List<Building> buildings, int goX, int goY, int cityX, int cityY, Passenger passenger)
        {
            int origXPos = car.XPos;
            int origYPos = car.YPos;
            Random rand = new Random();

            if (stuckCount > 0)
            {
                for (var i = 0; i < stuckCount; i++)
                {
                    switch (rand.Next(4))
                    {
                        case 1:
                            if (!Conflict(car, buildings, "y", 1)) { car.MoveUp(); }
                            break;
                        case 2:
                            if (!Conflict(car, buildings, "y", -1)) { car.MoveDown(); }
                            break;
                        case 3:
                            if (!Conflict(car, buildings, "x", 1)) { car.MoveRight(); }
                            break;
                        case 4:
                            if (!Conflict(car, buildings, "x", -1)) { car.MoveLeft(); }
                            break;
                        default:
                            if (!Conflict(car, buildings, "y", -1)) { car.MoveDown(); }
                            break;
                    }
                    car.WritePositionToConsole();
                }
            }

            for (int i = 0; i < car.Speed; i++)
            {
                if (car.XPos < goX)
                {
                    if (!Conflict(car, buildings, "x", 1)) { car.MoveRight(); }
                    if (car.XPos == goX) { continue; }
                }
                else if (car.XPos > goX)
                {
                    if (!Conflict(car, buildings, "x", -1)) { car.MoveLeft(); }
                    if (car.XPos == goX) { continue; }
                }
                else if (car.YPos < goY)
                {
                    if (!Conflict(car, buildings, "y", 1)) { car.MoveUp(); }
                    if (car.YPos == goY) { continue; }
                }
                else if (car.YPos > goY)
                {
                    if (!Conflict(car, buildings, "y", -1)) { car.MoveDown(); }
                    if (car.YPos == goY) { continue; }
                }

                if (origXPos == car.XPos && origYPos == car.YPos)
                {
                    stuckCount += 1;
                }
                else
                {
                    stuckCount = 0;
                }
            }
            car.WritePositionToConsole();
            CityPosition.Draw(cityX, cityY, car, passenger, buildings);

        }

        private static bool Conflict(Car car, List<Building> buildings, string axis, int increment)
        {
            int newXPos = car.XPos;
            int newYPos = car.YPos;

            if (axis == "x")
            {
                newXPos += increment;
            }
            else
            {
                newYPos += increment;
            }

            foreach (var building in buildings)
            {
                if (building.XPos == newXPos && building.YPos == newYPos)
                {
                    return true;
                }
            }

            return false;
        }

    }
}
