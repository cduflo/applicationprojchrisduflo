﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class RaceCar : Car
    { 

        public RaceCar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override int Speed
        {
            get
            {
                return 2;
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("RaceCar moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
