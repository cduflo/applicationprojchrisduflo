﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class SpaceShip : Car
    {

        public SpaceShip(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override int Speed
        {
            get
            {
                return 10;
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("SpaceShip moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
